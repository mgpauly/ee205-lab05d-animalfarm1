///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file main.h
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 1_Mar__2022
///////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#define MAX_CATS 1024
#define MAX_CAT_NAME 50
//#define DEBUG

// catDatabase
typedef enum {UNKNOWN_GENDER, MALE, FEMALE} catGender;
typedef enum {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX} catBreed;
typedef enum {BLACK, WHITE, RED, BLUE, GREEN, PINK, UNKNOWN_COLOR} Color;
struct cat {
    char name[MAX_CAT_NAME];
    catGender gender;
    catBreed breed;
    bool isFixed;
    float weight;
    Color collarColor1;
    Color collarColor2;
    unsigned long long license;
};